import * as React from 'react';
import { isString } from 'util';

interface FormProps {
  type?: string;
  className?: string;
  initialData?: { [key: string]: any };
  onValidate?: (inputName: string, value: string) => Promise<string | boolean>;
  onChange?: (data: any) => void;
  onSubmit?: (data: any) => void;
  children?: any;
}

const Form = (props: FormProps) => {

  const { type } = props;

  const formEl = React.useRef(null);

  const [data, setData] = React.useState(props.initialData ? props.initialData : {});
  const [errors, setErrors] = React.useState({});

  const [isValidated, setIsValidated] = React.useState(false);

  const clearError = (element: Element) => {
    element.className = element.className.replace(' has-error', '');
  };

  const setError = async (element: Element, errorMessage: string) => {
    clearError(element);
    element.className += ' has-error';
  };

  const validate = async () => {
    if (formEl && formEl.current && formEl.current instanceof HTMLFormElement) {
      // počet položek formuláře
      const formLength = (formEl.current as HTMLFormElement).length;
      const formItems = formEl.current;

      // chyba ve formuláři
      let customError = false;
      
      // lokální uložiště chyb
      const validateErrors = {};

      // vyčistím chybové hlášky položek formulářů
      for (let i = 0; i < formLength; i++) {
        const elem = formItems[i];
        const elemWrapper = elem.closest('.input-wrapper');
        const errorLabel = elemWrapper ? elemWrapper.querySelector('.invalid-feedback') : null;
        if (errorLabel) {
          errorLabel.textContent = '';
          clearError(elemWrapper);
        }
      }

      // kontrola validace položek formulářů
      for (let i = 0; i < formLength; i++) {
        // the i-th child of the form corresponds to the forms i-th input element
        const elem = formItems[i];
        const elemWrapper = elem.closest('.input-wrapper');
        const errorLabel = elemWrapper ? elemWrapper.querySelector('.invalid-feedback') : null;
        if (elem && errorLabel && elem.nodeName.toLowerCase() !== 'button') {
          if (!(elem as any).validity.valid) {
            errorLabel.textContent = (elem as any).validationMessage;
            setError(elemWrapper, (elem as any).validationMessage);
            validateErrors[(elem as any).name] = (elem as any).validationMessage;
            customError = true;

          } else {
            // vlastní validace formuláře
            if (props.onValidate) {
              const elementValid = await props.onValidate(elem.getAttribute('name'), elem.getAttribute('value'));
              if (elementValid && elementValid !== true && isString(elementValid)) {
                errorLabel.textContent = elementValid;
                setError(elemWrapper, elementValid);
                validateErrors[(elem as any).name] = elementValid;
                customError = true;
              }
            }
          }
        }
      }

      // nastavení objektu s chyami
      setErrors(validateErrors);

      // výstup
      return !customError;
    }

    return true;
  };

  const onChange = (e: any) => {
    const { name, value } = e.target;

    const newData = { ...data, [name]: value };

    setData(newData);

    if (props.onChange) {
      props.onChange(newData);
    }
  };

  const onSubmit = async (e: any) => {
    e.preventDefault();

    if (await validate() && props.onSubmit) {
      props.onSubmit(data);
    }

    setIsValidated(true);
  };

  const classNames: Array<string> = ['form'];
  if (type) {
    classNames.push(`form--${type}`);
  }
  if (isValidated) {
    classNames.push('was-validated');
  }

  return (
    <form
      className={`${classNames.join(' ')}`}
      noValidate={true}
      ref={formEl}
      onSubmit={onSubmit}
    >
      {props.children({
        data,
        errors,
        onChange,
        onSubmit,
      })}
    </form>
  );
};

export default Form;