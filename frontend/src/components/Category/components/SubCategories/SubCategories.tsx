import * as React from 'react';
import { useQuery } from 'react-apollo';

import { GetCategoriesQuery } from '@query/Categories/Categories';

import CategoryLink from '../CategoryLink';

interface SubCategoriesProps {
  category: GQL.ICategory;
}

const SubCategories = (props: SubCategoriesProps) => {

  const { category } = props;

  if (!category) {
    return null;
  }

  const categoriesQuery = useQuery(GetCategoriesQuery, {
    variables: {
      eshopId: process.env.REACT_APP_ESHOP_ID,
      where: {
        published: true,
      }
    },
  });

  const subCategories = categoriesQuery && categoriesQuery.data
    ? categoriesQuery.data.categories.filter((c: GQL.ICategory) => c.parent && c.parent.id === category.id)
    : [];

  if (!subCategories || subCategories.length === 0) {
    return null;
  }

  return (
    <div className="subCategories">
      <div className="row gutters-5">
        {subCategories.map((c: GQL.ICategory) => {
          return (
            <div key={c.id} className="col-12 col-sm-6 col-md-3">
              <div className="subCategories__item">
                <CategoryLink category={c}>
                  {c.title}
                </CategoryLink>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SubCategories;