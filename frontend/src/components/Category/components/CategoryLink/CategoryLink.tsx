import * as React from 'react';
import { Link } from '@reach/router';

interface CategoryLinkProps {
  category: GQL.ICategory;
  children?: any;
}

const CategoryLink = ({ category, children }: CategoryLinkProps) => {
  
  if (category && category.slug) {
    return (
      <Link to={`/category/${category.slug}`}>
        {children}
      </Link>
    );
  }

  return null;
};

export default CategoryLink;