import * as React from 'react';

import Helmet from '@components/Helmet';
import Catalog from '@components/Catalog';
import Breadcrumbs from '@components/Breadcrumbs';

import SubCategories from './components/SubCategories';

interface CategoryProps {
  category: GQL.ICategory;
}

const Category = (props: CategoryProps) => {

  const { category } = props;

  return (
    <>
      <Helmet
        name={category.name}
        title={category.title}
        pageTitle={category.pageTitle}
        description={category.description}
        keywords={category.keywords}
      />

      <section className="category">
        <div className="row">
          <div className="col-12">
            <Breadcrumbs
              category={category}
            />
          </div>
        </div>

        <h2>{category.title}</h2>

        {category.content &&
          <div
            className="category__description"
            dangerouslySetInnerHTML={{ __html: category.content }}
          />}

        <SubCategories
          category={category}
        />

        <Catalog
          category={category}
        />
      </section>
    </>
  );
}

export default Category;