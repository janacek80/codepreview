import * as React from 'react';
import { useQuery } from 'react-apollo';

import { GetCategoriesQuery } from '@query/Categories/Categories';

import CategoryListTreeComponent from './CategoryListTree';

const CategoryListTree = () => {

  const categoriesQuery = useQuery(GetCategoriesQuery, {
    variables: {
      eshopId: process.env.REACT_APP_ESHOP_ID,
      where: {
        published: true,
      }
    },
  });

  const categories = categoriesQuery && categoriesQuery.data
    ? categoriesQuery.data.categories
    : [];

  return (
    <CategoryListTreeComponent
      categories={categories}
    />
  );
};

export default CategoryListTree;