import * as React from 'react';

import CategoryLink from '@components/Category/components/CategoryLink';
import Image from '@modules/Image';

interface CategoryListTreeProps {
  categories: GQL.ICategory[];
}

const CategoryListTreeContent = (props: CategoryListTreeProps) => {

  const [opened, setOpened] = React.useState([]);

  const categories = [...props.categories];

  const isOpened = (categoryId: string) => {
    return opened.some(item => item === categoryId);
  };

  const openCategory = (categoryId: string) => {
    if (!isOpened(categoryId)) {
      const newOpened = [...opened];
      newOpened.push(categoryId);

      setOpened(newOpened);
    }
  };

  const renderTree = (parentId: string | null) => {
    const list = categories
      .filter((category: GQL.ICategory) => {
        return (parentId === null && category.parent === null) 
          || (parentId !== null && category.parent !== null && category.parent.id === parentId);
      })
      .map((category: GQL.ICategory, index: number) => {

        const subCategories = renderTree(category.id);

        const className = [];
        if (subCategories) {
          className.push('has-subcategories');
        }
        if (isOpened(category.id)) {
          className.push('opened');
        }

        return (
          <li
            key={index}
            className={className.join(' ')}
            onClick={() => openCategory(category.id)}
          >
            <CategoryLink category={category}>
              {category.title}
            </CategoryLink>
            {isOpened(category.id) &&
              subCategories}
          </li>);
      });

    if (list && list.length > 0) {
      return (<ul>{list}</ul>);
    }

    return null;
  };

  return (
    <div className="categories">
      <Image src={''} width={100} height={100} />

      <div className="categories__tree">
        {renderTree(null)}
      </div>
    </div>
  );
};

export default CategoryListTreeContent;