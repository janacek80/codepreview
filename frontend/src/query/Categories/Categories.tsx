import gql from 'graphql-tag';

export const GetCategoriesQuery: any = gql`
  query categories($eshopId: ID!, $where: CategoriesWhereInput) {
    categories(eshopId: $eshopId, where: $where) {
      id
      parent {
        id
        name
      }
      name
      slug
      title
      content
      pageTitle
      keywords
      description
      published
      priority
    }
  }
`;
