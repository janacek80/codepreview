import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, ManyToMany } from 'typeorm';
import { Eshop } from 'portfolio/eshop/eshop.entity';
import { Product } from 'portfolio/product/product.entity';

@Entity('category')
export class Category {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Eshop, { onDelete: 'CASCADE' })
  eshop: Eshop;

  @ManyToOne(type => Category, { onDelete: 'SET NULL' })
  parent: Category;

  @ManyToMany(type => Product, product => product.categories)
  products: [Product];

  @Column({ default: true })
  published: boolean;

  @Column()
  name: string;
  
  @Column({ type: 'jsonb' })
  title: any;
  
  @Column({ type: 'jsonb' })
  slug: any;
  
  @Column({ type: 'jsonb', nullable: true })
  pageTitle: any;
  
  @Column({ type: 'jsonb', nullable: true })
  description: any;
  
  @Column({ type: 'jsonb', nullable: true })
  keywords: any;
  
  @Column({ type: 'jsonb', nullable: true })
  content: any;
  
  @Column({ type: 'int', nullable: true })
  priority: number;
  
  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @Column({ default: false })
  deleted: boolean;
}