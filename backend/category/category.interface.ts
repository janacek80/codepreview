import { Eshop } from '../eshop/eshop.entity';

export interface Category {
  id: string;
  eshop: Eshop;
  parent: Category;
  published: boolean;
  name: string;
  title: any;
  slug: any;
  pageTitle: any;
  description: any;
  keywords: any;
  content: any;
  priority: number;
  createdAt: string;
  updatedAt: string;
  deleted: boolean;
}

export interface CreateCategoryInput {
  eshopId: string;
  parentId: string;
  published: boolean;
  name: string;
  title: any;
  slug: any;
  pageTitle: any;
  description: any;
  keywords: any;
  content: any;
  priority: number;  
}

export interface UpdateCategoryInput {
  parentId: string;
  published: boolean;
  name: string;
  title: any;
  slug: any;
  pageTitle: any;
  description: any;
  keywords: any;
  content: any;
  priority: number;  
}
