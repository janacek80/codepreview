import { Mutation, Query, Resolver } from '@nestjs/graphql';
import { CategoryService } from './category.service';
import { Not } from 'typeorm';

@Resolver()
export class CategoryResolver {

  constructor(
    private readonly categoryService: CategoryService,
  ) {
  }

  @Query()
  public async categories(obj, args) {
    return await this.categoryService.findAll({
      where: {
        eshop: { id: args.eshopId },
        deleted: Not(true),
      },
    });
  }

  @Query()
  public async category(obj, args) {
    return await this.categoryService.findOne({
      where: {
        eshop: { id: args.eshopId },
        id: args.where.id,
        deleted: Not(true),
      },
    });
  }

  @Mutation()
  public async createCategory(obj, args) {
    return await this.categoryService.create(args.data);
  }

  @Mutation()
  public async updateCategory(obj, args) {
    return await this.categoryService.update(args.id, args.data);
  }

  @Mutation()
  public async deleteCategory(obj, args) {
    await this.categoryService.delete(args.id);
    return { id: args.id };
  }
}
