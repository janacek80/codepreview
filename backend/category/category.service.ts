import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindManyOptions, FindOneOptions, Not } from 'typeorm';
import { Category } from './category.entity';
import { CreateCategoryInput, UpdateCategoryInput } from './category.interface';
import { updateSlugInput } from 'helpers/slug/updateSlugInput';
import { normalizeTypeOrmWhere } from 'helpers/orm/normalizeTypeOrmWhere';

@Injectable()
export class CategoryService {
  
  public constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {
  }

  public findAll(options: FindManyOptions<Category>): Promise<Category[]> {
    return this.categoryRepository.find({
      order: {
        createdAt: 'DESC',
      },
      ...options,
      where: normalizeTypeOrmWhere(
        options.where, {
          deleted: Not(true)
        }
      ),
      relations: ['parent'],
    });
  }

  public findOne(options: FindOneOptions<Category>): Promise<Category> {
    return this.categoryRepository.findOne({
      order: {
        createdAt: 'DESC',
      },
      ...options,
      where: normalizeTypeOrmWhere(
        options.where, {
          deleted: Not(true)
        }
      ),
      relations: ['parent'],
    });
  }

  // pouze pro interní účely
  // ignoruje kolizi v detekci záznamu podle ID z jiném eshopu
  private findOneById(id: string): Promise<Category> {
    return this.findOne({
      where: { id }
    });
  }

  public async create(data: CreateCategoryInput): Promise<Category> {
    const newData = { ...data };

    // eshop
    (newData as any).eshop = data.eshopId ? { id: data.eshopId } : null;
    delete(newData.eshopId);

    // nadřazená kategorie
    (newData as any).parent = data.parentId ? { id: data.parentId } : null;
    delete(newData.parentId);
  
    // url aliasy
    newData.slug = await this.updateCategorySlug(newData);

    // uložení
    const result = await this.categoryRepository.insert({
      ...newData as any,
    });

    const categoryId = result.identifiers[0].id;

    return this.findOneById(categoryId);
  }

  public async update(id: string, data: UpdateCategoryInput): Promise<Category> {
    const newData = { ...data };

    // nadřazená kategorie
    (newData as any).parent = data.parentId !== null ? { id: data.parentId } : null;
    delete newData.parentId;

    // aktualizace
    await this.categoryRepository.update(
      { id },
      { ...newData }
    );

    return await this.findOneById(id);
  }

  public async delete(id: string): Promise<boolean> {
    await this.categoryRepository.update(id, {
      deleted: true,
    });

    return true;
  }

  // todo: doladit !!!
  private updateCategorySlug = async (category: any) => {
    console.log('updateCategorySlug category', category);
    
    // aliasy z dat kategorie
    let slug = category.slug;
    if (!slug) {
      slug = { ...category.title };
    }
    slug = updateSlugInput(slug);
  
    // todo: kontrola unikátnosti
    for (const lang of Object.keys(slug)) {
      console.log('slug', lang, slug[lang]);
    }
    
    // výstup
    return slug;
  }
}
