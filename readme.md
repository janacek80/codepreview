## Backend

Na backendu používám Node.js ve spojení s Nest.js

Pro napojení na DB (Postgres) používám TypeORM

Ve zdrojácích je kompletní nastavení modulu kategorie, který obsluhuje jak strukturu tabulky v DB, tak i komunikaci s frontendem. Jednotlivé veřejné GraphQL funkce zpracovávání běžné akce.

## Frontend

Na frontendu je klasický ReactJS

Ve zdrojácích jsou komponenty pro vykreslení detailu vybrané kategorie, vč. drobečkové navigace, výpisu podkategorií, popisu a komponenty pro seznam produktů v kategorii. Dále je tam taková minikomponenta, který centralizuje generování odkazu na kategorii

Dále je tam menší query komponenta, která se stará o komunikaci (zjištení dat kategorií) frontendu s backendem

Dále je tam modul formuláře, který se stará víceméně o automatickou validaci formulářů, vč. výpisu chyb u jednotlivých inputů apod. 

